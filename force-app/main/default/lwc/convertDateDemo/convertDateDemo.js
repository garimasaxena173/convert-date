import { LightningElement } from 'lwc';

export default class ConvertDateDemo extends LightningElement {

    public static String convertDate(Date d) {
        
        String convertedDate = '';

        if (d != null) {
            switch on d.month() {
                when 1 {
                    convertedDate += 'Jan ';
                }
                when 2 {
                    convertedDate += 'Feb ';
                }
                when 3 {
                    convertedDate += 'Mar ';
                }
                when 4 {
                    convertedDate += 'Apr ';
                }
                when 5 {
                    convertedDate += 'May ';
                }
                when 6 {
                    convertedDate += 'June ';
                }
                when 7 {
                    convertedDate += 'July ';
                }
                when 8 {
                    convertedDate += 'Aug ';
                }
                when 9 {
                    convertedDate += 'Sep ';
                }
                when 10 {
                    convertedDate += 'Oct ';
                }
                when 11 {
                    convertedDate += 'Nov ';
                }
                when 12 {
                    convertedDate += 'Dec ';
                }
            }
            convertedDate += d.day() + ', ' + d.year();
        }
        return convertedDate;
    }

}